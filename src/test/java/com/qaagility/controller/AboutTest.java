package com.qaagility.controller;
import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.ui.ModelMap;

public class AboutTest {

        @Test
	public void MatchStringFound() throws Exception {
		About about = new About();

		String Actual = about.desc();
		String Expected = "This application was copied from somewhere, sorry but I cannot give the details and the proper copyright notice. Please don't tell the police!";
                assertTrue("nnnn",Expected.equals(Actual));
	        assertEquals("", Expected,Actual);
	}
}
