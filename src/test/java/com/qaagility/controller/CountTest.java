package com.qaagility.controller;
import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.ui.ModelMap;

public class CountTest {
    
    @Test
    public void testDigits() {
        Count count = new Count();
        
        int result = count.digits(10, 5);
        assertEquals(2, result);
    }

    @Test
    public void testDigitsDivideByZero() {
        Count count = new Count();
        
        int result = count.digits(10, 0);
        assertEquals(Integer.MAX_VALUE, result);
    }
}







